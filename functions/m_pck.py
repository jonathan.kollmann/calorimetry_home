import os
import sys
import time
import json

from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np


# This if statement enables you to write and run programs that test functions directly at the end of this file.
if __name__ == "__main__":
    import pathlib

    file_path = os.path.abspath(__file__)
    file_path = pathlib.Path(file_path)
    root = file_path.parent.parent
    sys.path.append(str(root))

from functions import m_json


def check_sensors() -> None:
    """Retrieves and prints the serial number and current temperature of all DS18B20 Temperature Sensors.

    This function utilizes the `w1thermsensor` library to interface with the DS18B20 temperature sensors.
    For each available sensor, it prints its serial number (or ID) and the current temperature reading.

    Examples:
    Assuming two DS18B20 sensors are connected:
    >>> check_sensors()
    Sensor 000005888445 has temperature 25.12
    Sensor 000005888446 has temperature 24.89

    """

    for Sensor in W1ThermSensor.get_available_sensors():
        print("Sensor %s has temperature %.2f" % (Sensor.id, Sensor.get_temperature()))


def get_meas_data_calorimetry(metadata: dict) -> dict:
    """Collects and returns temperature measurement data from DS18B20 sensors based on the provided metadata.

    This function initializes sensor objects based on the metadata, prompts the user to start the
    measurement, and then continually reads temperature values until interrupted (e.g., via Ctrl-C).
    It logs the temperatures and the corresponding timestamps. Refer to README.md section
    "Runtime measurement data" for a detailed description of the output data structure.

    Args:
        metadata (dict): Contains details like sensor uuid, serials, and names.
                         Refer to README.md section "Runtime metadata" for a detailed description
                         of the input data structure.

    Returns:
        dict: A dictionary with sensor uuid as keys, and corresponding lists of temperatures and timestamps.

    Example:
        Input metadata:
        {
            "sensor": {
                "values": ["sensor_1", "sensor_2"],
                "serials": ["000005888445", "000005888446"],
                "names": ["FrontSensor", "BackSensor"]
            }
        }

        Output (example data after interruption):
        {
            "sensor_1": [[25.12, 25.15], [0, 2]],
            "sensor_2": [[24.89, 24.92], [0, 2]]
        }

    """
    # Initialize an empty dictionary for storing temperature measurements.
    # The structure is uuid: [[temperatures], [timestamps]].
    data = {i: [[], []] for i in metadata["sensor"]["values"]}
    sensor_list = [W1ThermSensor(Sensor.DS18B20, id) for id in metadata["sensor"]["serials"]]
    input("Press any key to start measurement... <Ctrl+C> to stop measurement")
    start = time.time()
    try:
        while True:
            for i, sensor in enumerate(sensor_list):
                # Die Zeit- und Temperaturdaten werden für die beiden Sensoren jeweils in einer Liste
                # in data gespeichert, welcher anschließend als ein Datenblock zurückgegeben wird.
                if i == 0:
                    data[metadata['sensor']['values'][0]][i].append(sensor.get_temperature())
                    data[metadata['sensor']['values'][0]][i+1].append(round(time.time(),0))
                else:
                    data[metadata['sensor']['values'][1]][i-1].append(sensor.get_temperature())
                    data[metadata['sensor']['values'][1]][i].append(round(time.time(),0))
            # Print an empty line for better readability in the console.
            print("")
    # Catch the KeyboardInterrupt (e.g., from Ctrl-C) to stop the measurement loop.
    except KeyboardInterrupt:
        # Print the collected data in a formatted JSON structure.
        print(json.dumps(data, indent=4))
    # Always execute the following block.
    finally:
        # Ensure that the lengths of temperature and timestamp lists are the same for each sensor.
        for i in data:
            # If the temperature list is longer, truncate it to match the length of the timestamp list.
            if len(data[i][0]) > len(data[i][1]):
                data[i][0] = data[i][0][0 : len(data[i][1])]
            # If the timestamp list is longer, truncate it to match the length of the temperature list.
            elif len(data[i][0]) < len(data[i][1]):
                data[i][1] = data[i][1][0 : len(data[i][0])]

    return data


def logging_calorimetry(
    data: dict,
    metadata: dict,
    data_folder: str,
    json_folder: str,
) -> None:
    """Logs the calorimetry measurement data into an H5 file.

    This function creates a folder (if not already present) and an H5 file with a
    specific structure. The data from the provided dictionaries are written to the
    H5 file, along with several attributes.

    Args:
        data (dict): Contains sensor data including temperature and timestamp.
                     Refer to README.md section"Runtime measurement data" for a detailed
                     description of the data structure.
        metadata (dict): Contains metadata. Refer to README.md section "Runtime metadata"
                         for a detailed description of the structure.
        data_folder (str): Path to the folder where the H5 file should be created.
        json_folder (str): Path to the folder containing the datasheets.

    """
    # Extract the folder name from the provided path to be used as the H5 file name.
    log_name = data_folder.split("/")[-1]
    # Generate the full path for the H5 file.
    dataset_path = "{}/{}.h5".format(data_folder, log_name)
    # Check and create the logging folder if it doesn't exist.
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    # Create a new H5 file.
    f = h5py.File(dataset_path, "w")
    # Create a 'RawData' group inside the H5 file.
    grp_raw = f.create_group("RawData")
    
    # TODO: Add attribute to HDF5.
    # Set attributes for the H5 file based on the datasheets.
    f.attrs["created"] = '19.11.2023'
    f.attrs["experiment"] = 'Kalorimetrie Küchentisch'
    f.attrs["group_number"] = '49'
    f.attrs["authors"] = 'Jonathan Kollmann'
    # DONE #
    # TODO: Write data to HDF5.
    # Gruppen mit Sensor-UUIDs als Bezeichnung werden erstellt und Attribute Name sowie Seriennummer hinzugefügt.
    sensor_1 = grp_raw.create_group(list(data.keys())[0])
    sensor_1.attrs["name"] = "Temperature_Sensor"
    sensor_1.attrs["serial"] = "3ce10457714d"
    sensor_2 = grp_raw.create_group(list(data.keys())[1])
    sensor_2.attrs["name"] = "Temperature_Sensor"
    sensor_2.attrs["serial"] = "3ce1045759e8"
    
    # Einlesen der Temperatur -und Zeitwerte aus dem data-Datenblock
    temp_1 = list(data.values())[0][0]
    time_1 = list(data.values())[0][1]
    temp_2 = list(data.values())[1][0]
    time_2 = list(data.values())[1][1]
    
    # Abspeichern der Temperatur- und Zeitwerte in der H5-Datei sowie Hinzufügen der Attribute zu den Einheiten
    temp_s1 = sensor_1.create_dataset('temperature', data=temp_1)
    temp_s1.attrs["description"] = "in degree celsius"
    time_s1 = sensor_1.create_dataset('timestamp', data=time_1)
    time_s1.attrs["description"] = "in seconds"
    time_s1.attrs["time_convention"] = "Unixzeit"
    time_s1.attrs["the_epoch"] = "Donnerstag, der 1. Januar 1970, 00:00 Uhr UTC"
    time_s1.attrs["time_explanation"] = "Die Unixzeit zaehlt die vergangenen Sekunden seit The Epoch"
    temp_s2 = sensor_2.create_dataset('temperature', data=temp_2)
    temp_s2.attrs["description"] = "in degree celsius"
    time_s2 = sensor_2.create_dataset('timestamp', data=time_2)
    time_s2.attrs["description"] = "in seconds"
    time_s2.attrs["time_convention"] = "Unixzeit"
    time_s2.attrs["the_epoch"] = "Donnerstag, der 1. Januar 1970, 00:00 Uhr UTC"
    time_s2.attrs["time_explanation"] = "Die Unixzeit zaehlt die vergangenen Sekunden seit The Epoch"    
    # DONE #

    # Close the H5 file.
    f.close()


if __name__ == "__main__":
    # Test and debug.
    pass
