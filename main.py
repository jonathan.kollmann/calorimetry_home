from functions import m_json
from functions import m_pck

# Sensor-Check
# gibt die Sensor_ID und den aktuellen Temperaturwert aus,
# wenn die Sensoren richtig angeschlossen und betriebsbereit sind.
m_pck.check_sensors()

# Metadaten der Versauche werden eingelesen. Bei den Sensoren werden die Seriennummern ergänzt
path_1 = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
path_2 = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
metadata_1 = m_json.get_metadata_from_setup(path_1)
metadata_2 = m_json.get_metadata_from_setup(path_2)
path_sensor_1 = "/home/pi/calorimetry_home/datasheets/sensor_1.json"
path_sensor_2 = "/home/pi/calorimetry_home/datasheets/sensor_2.json"
metadata_1 = m_json.add_temperature_sensor_serials(path_sensor_1, metadata_1)
metadata_1 = m_json.add_temperature_sensor_serials(path_sensor_2, metadata_1)
metadata_2 = m_json.add_temperature_sensor_serials(path_sensor_1, metadata_2)
metadata_2 = m_json.add_temperature_sensor_serials(path_sensor_2, metadata_2)
print(metadata_1)
print(metadata_2)

# Die Messungen für die beiden Verasuche werden ausgeführt
# und in einer hdf5-Datei im data-Ordner der Abgabe gespeichert
measurement_1 = m_pck.get_meas_data_calorimetry(metadata_1)
# print(measurement_1) # kann aktiviert werden, um sich die Messwerte direkt anzusehen
capacity = m_pck.logging_calorimetry(measurement_1, metadata_1,
                                     '/home/pi/calorimetry_home/data/heat_capacity',
                                     '/home/pi/calorimetry_home/datasheets')
                                     
measurement_2 = m_pck.get_meas_data_calorimetry(metadata_2)
# print(measurement_2) # kann aktiviert werden, um sich die Messwerte direkt anzusehen
newton = m_pck.logging_calorimetry(measurement_2, metadata_2,
                                   '/home/pi/calorimetry_home/data/newton',
                                   '/home/pi/calorimetry_home/datasheets')
                                

# JSON-Dateien über die Versuchs-Setups in die data-Ordner der Versuche übertragen.
m_json.archiv_json('/home/pi/calorimetry_home/datasheets', '/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json',
                   '/home/pi/calorimetry_home/data/heat_capacity')

m_json.archiv_json('/home/pi/calorimetry_home/datasheets', '/home/pi/calorimetry_home/datasheets/setup_newton.json',
                   '/home/pi/calorimetry_home/data/newton')                               